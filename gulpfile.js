(function (gulp, gulpLoadPlugins, moment, karma) {
	'use strict';

	var $ = gulpLoadPlugins({ pattern: '*', lazy: true }),
	_ = { app: 'app', dist: 'dist' };

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ jsonlint
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('jsonlint', function() {
		return gulp.src([
			'package.json',
			'bower.json',
			'.bowerrc',
			'.jshintrc',
			'.jscs.json'
		])
		.pipe($.plumber())
		.pipe($.jsonlint()).pipe($.jsonlint.reporter());
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ jshint
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('jshint', function() {
		return gulp.src([
			'gulpfile.js',
			_.app + '/scripts/**/*.js',
			'!' + _.app + '/scripts/vendor/**/*.js',
			'!' + _.app + '/scripts/utils/**/*.js',
			'test/spec/{,*/}*.js'
		])
		.pipe($.plumber())
		.pipe($.jshint('.jshintrc')).pipe($.jshint.reporter('default'))
		.pipe($.jscs());
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ karma
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('karma', function () {
		var server = new karma.Server({
			configFile: __dirname + '/karma.conf.js',
			singleRun: true
		});

		server.start();
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ requirejs
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('requirejs', ['jshint'], function() {
		$.requirejs({
			baseUrl: _.app + '/scripts',
			optimize: 'none',
			include: ['requirejs', 'config'],
			mainConfigFile: _.app + '/scripts/config.js',
			out: 'body.js',
			preserveLicenseComments: true,
			useStrict: true,
			wrap: true
		})
		.pipe($.plumber()).pipe(gulp.dest(_.dist + '/scripts')).pipe($.size());
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ scripts
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('scripts', ['requirejs'], function() {
		return gulp.src([
			_.app + '/scripts/**/*.js',
			'!' + _.app + '/scripts/vendor/**/*.js'
		]).pipe($.plumber()).pipe($.size());
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ styles
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('styles', function () {
		return gulp.src(_.app + '/styles/theme.scss')
		.pipe($.sass.sync().on('error', $.sass.logError))
		.pipe($.autoprefixer('last 1 version', '> 1%', 'ie 8'))
		.pipe(gulp.dest(_.app + '/styles'))
		.pipe($.size());
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ svg
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('svg', function() {
		return gulp.src([
			_.app + '/images/**/*.svg',
			_.app + '/styles/**/*.svg'
		])
		.pipe($.plumber())
		.pipe($.svgmin([{ removeDoctype: false }, { removeComments: false }]))
		.pipe(gulp.dest(_.dist + '/images'))
		.pipe($.size());
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ images
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('images', function() {
		gulp.src(_.app + '/*.{png,jpg,jpeg,gif,ico}').pipe(gulp.dest(_.dist));
		return gulp.src([
			_.app + '/images/**/*.{png,jpg,jpeg,gif,ico}'
		])
		.pipe($.plumber())
		.pipe($.imagemin({
			optimizationLevel: 3,
			progressive: true,
			interlaced: true
		}))
		.pipe(gulp.dest(_.dist + '/images'))
		.pipe($.size());
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ html
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('html', ['styles', 'scripts'], function() {
		var js = $.filter('**/*.js'), css = $.filter('**/*.css');
		gulp.src(_.app + '/*.txt').pipe(gulp.dest(_.dist));
		return gulp.src([_.app + '/*.html']).pipe($.plumber())
		.pipe($.useref.assets())
		.pipe(js)
		.pipe($.uglify())
		.pipe(js.restore())
		.pipe(css)
		.pipe($.csso())
		.pipe(css.restore())
		.pipe($.useref.assets().restore())
		.pipe($.useref())
		.pipe(gulp.dest(_.dist))
		.pipe($.size());
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ bower (Inject Bower components)
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('wiredep', function() {
		gulp.src(_.app + '/styles/*.{sass,scss}').pipe($.wiredep.stream({
			directory: _.app + '/scripts/vendor',
			ignorePath: _.app + '/scripts/vendor/'
		})).pipe(gulp.dest(_.app + '/styles'));
		gulp.src(_.app + '/*.html').pipe($.wiredep.stream({
			directory: _.app + '/scripts/vendor',
			ignorePath: _.app + '/'
		})).pipe(gulp.dest(_.app));
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ connect
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('connect', $.connect.server({
		root: [_.app],
		livereload: true,
		port: 9000
	}));

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ server
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('server', ['connect', 'styles'], function() {
		gulp.start('localhost');
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ watch
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('watch', ['server'], function() {
		// Watch for changes in `app` dir
		$.watch({ glob: [
			_.app + '/*.{html,txt}',
			_.app + '/styles/**/*.{sass,scss,css}',
			_.app + '/scripts/**/*.js',
			_.app + '/images/**/*.{png,jpg,jpeg,gif,ico}',
			'!' + _.app + '/scripts/vendor/**/*.js'
		] }, function(files) {
			return files.pipe($.plumber()).pipe($.connect.reload());
		});

		// Watch style files
		$.watch({ glob: [_.app + '/styles/**/*.{sass,scss}'] }, function() {
			gulp.start('styles');
		});

		// Watch script files
		$.watch({ glob: [_.app + '/scripts/**/*.js', '!' + _.app + '/scripts/vendor/**/*.js'] }, function() {
			gulp.start('scripts');
		});

		// Watch image files
		$.watch({ glob: [_.app + '/images/**/*.{png,jpg,jpeg,gif,ico}'] }, function() {
			gulp.start('images');
		});

		// Watch bower files
		$.watch({ glob: 'bower.json' }, function() {
			gulp.start('wiredep');
		});
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ clean
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('clean', [], function() {
		var stream = gulp.src([
			_.dist + '/images',
			_.dist + '/scripts',
			_.dist + '/styles'
		], { read: false });
		return stream.pipe($.clean());
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ environ
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('localhost', function() {
		$.shelljs.exec('open http://localhost:9000');
	});

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ alias
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('test', ['jsonlint', 'jshint', 'karma']);
	gulp.task('build', ['test', 'html', 'images', 'svg']);

	//|**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//| ✓ default
	//'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	gulp.task('default', ['clean'], function() {
		gulp.start('build');
	});

}(require('gulp'), require('gulp-load-plugins'), require('momentjs'), require('karma')));
