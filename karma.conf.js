// karma.conf.js
module.exports = function(config) {
  config.set({
    frameworks: ['jasmine', 'requirejs'],
    reporters: ['spec'],
    browsers: ['PhantomJS'],
    files: [
      'test-main.js',
      'node_modules/promise-polyfill/promise.js',
      {pattern: 'app/scripts/charts/spec/*.js', included: false},
      {pattern: 'app/scripts/charts/*.js', included: false},
      {pattern: 'app/scripts/*.js', included: false},
      {pattern: 'app/scripts/utils/backend/*.js', included: false},
      {pattern: 'app/scripts/utils/d3/*.js', included: false}
    ],
    paths: {
    }
  });
};
