//|**
//|
//| This file is the main application file
//|
//'*/
require.config({
	deps: ['main'],
	waitSeconds: 45,
	paths: {
		requirejs: 'vendor/requirejs/require',
		d3: 'utils/d3/d3.min',
		backend: 'utils/backend/backend',
	},
	shim: {
	}
});
