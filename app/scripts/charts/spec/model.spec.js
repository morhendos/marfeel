define(['charts/model'], function(Model) {
  'use strict';

  describe('Charts model', function() {

    var model = new Model('charts');

    describe('when getAll method is triggered', function() {

      var promise = model.getAll();

      it('should be able to make http request', function() {
        expect(promise).toEqual(jasmine.any(Promise));
      });

      it('should return data', function(done) {
        promise.then(function (response) {
          expect(response).not.toBeUndefined();
          expect(response).toEqual(jasmine.any(String));
          done();
        });
      });

      it('returned data should be not empty json', function(done) {
        promise.then(function (response) {
          expect(JSON.parse(response)).toEqual(jasmine.any(Object));
          expect(JSON.parse(response)[model.name].length).toBeGreaterThan(0);
          done();
        });
      });

    });

  });
});
