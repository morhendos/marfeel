define(['charts/controller','charts/model','charts/view'], function(Controller, Model, View) {
  'use strict';

  describe('Charts controller', function() {

    var controller = new Controller(new Model('charts'), new View('charts'));

    describe('when init method is triggered', function() {

      controller.init();

      it('controller scope should be filled with data provided by the model', function() {
        expect(controller.scope[controller.model.name]).toBeTruthy();
      });

      it('data provided by the model should not be empty', function() {
        expect(controller.scope.charts.length).toBeGreaterThan(0);
      });

    });

    describe('when listAll method is triggered', function() {

      beforeEach(function() {
        controller.listAll();

        spyOn(controller.view, 'renderElement');
      });

    });

  });
});
