define(function() {
  'use strict';

  return {
    charts: [
      {
        id: 1,
        name: 'Revenue',
        value: '200.000 &euro;',
        parts: [
          {
            label: 'Smartphone',
            value: '80.000 &euro;',
            count: 0.4,
            color: '#347322'
          },
          {
            label: 'Tablet',
            value: '120.000 &euro;',
            count: 0.6,
            color: '#54D326'
          }
        ],
      },
      {
        id: 2,
        name: 'Impresions',
        value: '50.000.000',
        parts: [
          {
            label: 'Smartphone',
            value: '30.000.000',
            count: 0.6,
            color: '#004F65'
          },
          {
            label: 'Tablet',
            value: '20.000.000',
            count: 0.4,
            color: '#03C9E2'
          }
        ],
      },
      {
        id: 3,
        name: 'Visits',
        value: '600.000.000',
        parts: [
          {
            label: 'Smartphone',
            value: '120.000.000',
            count: 0.2,
            color: '#D75516'
          },
          {
            label: 'Tablet',
            value: '480.000.000',
            count: 0.8,
            color: '#F7C72A'
          }
        ],
      }
    ]
  };

});
