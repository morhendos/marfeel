define(function() {
  'use strict';

  function ChartsController(model, view) {
    this.model = model;
    this.view = view;
    this.scope = [];
  }

  ChartsController.prototype.init = function() {
    var _this = this;
    _this.model.getAll().then(function (response) {
      _this.scope = JSON.parse(response);
      _this.listAll();
    })
    .catch (function(response) {
      console.log('error', response);
    });

  };

  ChartsController.prototype.listAll = function() {
    var _this = this,
    charts = _this.scope.charts,
    chartsLength = charts.length,
    i;

    for (i = 0; i < chartsLength; i++) {
      _this.view.renderElement(charts[i]);
    }
  };

  return ChartsController;
});
