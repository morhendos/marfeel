define(['backend', 'charts/data'], function(backend, data) {
  'use strict';

  function Model(name) {

    this.name = name;

    /**
     * mocked communication with fake server
     * listen for 'http://api/charts'
     * return data from data.js file when GET request detected
     */
    backend.when('GET', 'http://api/' + this.name).respond(data);
  }

  /**
   * makes HTTP request to server
   * @param {string} method - HTTP request method
   * @param {string} url - endpoint url
   */
  function makeRequest(method, url) {
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();
      xhr.open(method, url, false);
      xhr.onload = function() {
        resolve({
          target: {
            response: xhr.response
          }
        });
      };
      xhr.onerror = reject;
      xhr.send();
    });
  }

  /**
   * 'get all' method :
   * it uses makeRequest method,
   * adds model name provided as argument in Model constructor to request url,
   * if request is successful returns list of all items from rest api in json format
   */
  Model.prototype.getAll = function getAll() {
    return makeRequest('GET', 'http://api/' + this.name)
    .then(
      function (e) {
        return e.target.response;
      },
      function (e) {
        throw e;
      }
    );
  };

  return Model;
});
