/* global define */
define(['d3'], function(d3) {
	'use strict';

	function View(elementId) {
		this.listElement = document.getElementById(elementId);
		this.defaultTemplate =
		'<li class="chart" id="chart-{{id}}">' +
				'<div class="chart-svg"></div>' +
				'<div class="label">{{name}}</div>' +
				'<div class="value">{{value}}</div>' +
				'<div class="details">' +
						'<div class="left">' +
							'<div class="label">{{label-a}}</div>' +
							'<div class="value">' +
									'<span class="percent">{{percent-a}}</span>{{value-a}}' +
							'</div>' +
					'</div>' +
					'<div class="right">' +
							'<div class="label">{{label-b}}</div>' +
							'<div class="value">' +
									'<span class="percent">{{percent-b}}</span>{{value-b}}' +
							'</div>' +
					'</div>' +
				'</div>' +
		'</li>';
	}

	function injectValuesIntoTemplate(data, template) {
		template = template.replace('{{id}}', data.id);
		template = template.replace('{{name}}', data.name);
		template = template.replace('{{value}}', data.value);
		template = template.replace('{{label-a}}', data.parts[1].label);
		template = template.replace('{{value-a}}', data.parts[1].value);
		template = template.replace('{{percent-a}}', data.parts[1].count * 100 + '%');
		template = template.replace('{{label-b}}', data.parts[0].label);
		template = template.replace('{{value-b}}', data.parts[0].value);
		template = template.replace('{{percent-b}}', data.parts[0].count * 100 + '%');

		return template;
	}

	function setLabelsColors(data) {
		d3.select('#chart-' + data.id + ' .right .label').style('color', data.parts[0].color);
		d3.select('#chart-' + data.id + ' .left .label').style('color', data.parts[1].color);
	}

	function animatePieChartData(data, path, pie, arc) {
		var dataset = [
			{ label: 'empty', count: 0 },
			data[0],
			data[1]
		];

		path = path.data(pie(dataset));

		path.transition()
		.duration(3000)
		.attrTween('d', function(d) {
			var interpolate = d3.interpolate(this._current, d);
			this._current = interpolate(0);
			return function(t) {
				return arc(interpolate(t));
			};
		});
	}

	function renderPieChart(params) {
		var dataset, width, height, radius, donutWidth, color, svg, arc, pie, path;

		dataset = [
			{ label: 'empty', count: 1 },
			{ label: 'smartphone', count: 0 },
			{ label: 'tablet', count: 0 }
		];

		width = 161;
		height = 161;
		radius = Math.min(width, height) / 2;
		donutWidth = 7;
		color = d3.scaleOrdinal().range(['#FFF', params.parts[0].color, params.parts[1].color]);

		svg = d3.select('#chart-' + params.id + ' .chart-svg')
		.append('svg')
		.attr('width', width)
		.attr('height', height)
		.append('g')
		.attr('transform', 'translate(' + (width / 2) +  ',' + (height / 2) + ')');

		arc = d3.arc()
		.innerRadius(radius - donutWidth)
		.outerRadius(radius);

		pie = d3.pie()
		.value(function(d) { return d.count; })
		.sort(null);

		path = svg.selectAll('path')
		.data(pie(dataset))
		.enter()
		.append('path')
		.attr('d', arc)
		.attr('fill', function(d) {
			return color(d.data.label);
		})
		.each(function(d) { this._current = d; });

		animatePieChartData([params.parts[0], params.parts[1]], path, pie, arc);
	}

	View.prototype.renderElement = function(data) {
		var template = injectValuesIntoTemplate(data, this.defaultTemplate);
		if (this.listElement) {
			this.listElement.insertAdjacentHTML('beforeend', template);
		}
		setLabelsColors(data);
		renderPieChart(data);
	};

	return View;
});
