require(['charts/model', 'charts/view', 'charts/controller'], function (Model, View, Controller) {
    'use strict';

    var controller = new Controller(new Model('charts'), new View('charts'));
    controller.init();
});
