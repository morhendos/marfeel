####################################################################
## This is client side MVC web application in vanilla js
## ----------------------------------------------------------------
## - requirejs is being used for dependencies management
## - karma with jasmine for unit tests
## - D3JS library for graphic features
## - gulp for automating tasks
## ----------------------------------------------------------------
## Author: Tomasz Mikolajewicz / morhendos@gmail.com
####################################################################


Run `npm install` for installing packages.
Run `gulp server` for running app in local server (localhost:9000)
Run `gulp test` for unit tests.
